package com.carlostorres.poststest

import androidx.annotation.NonNull
import com.carlostorres.poststest.model.Post
import com.carlostorres.poststest.model.database.PostDao
import com.carlostorres.poststest.remote.PostApi
import com.carlostorres.poststest.view.posts.viewmodels.PostListViewModel
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class PostsUnitTest {

    private lateinit var viewModel: PostListViewModel

    @Mock
    var postDao: PostDao? = null
    @Mock
    private lateinit var service: PostApi
    @Mock
    var observer: Observer<List<Post>>? = null

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUpRxSchedulers() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Scheduler.Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        postDao?.let { viewModel = PostListViewModel(it) }
    }

    @Test
    fun testNull() {
        `when`(service.getPosts()).thenReturn(null)
        assertNotNull(viewModel.loadPosts())
    }

    @Test
    fun testApiFetchDataSuccess() {
        `when`(service.getPosts()).thenReturn(Observable.just(listOf()))
        viewModel.loadPosts()
    }
}
